Source: blitz++
Maintainer: Debian Math Team <team+math@tracker.debian.org>
Uploaders: Jerome Benoit <calculus@rezozer.net>,
           Christophe Trophime <christophe.trophime@lncmi.cnrs.fr>
Section: devel
Priority: optional
Build-Depends: dpkg-dev (>= 1.22.5),
 debhelper-compat (= 13),
 autoconf-archive, automake, libtool, python3,
 gfortran | fortran-compiler, cfortran,
 libblas-dev | libatlas-base-dev,
 liblapack-dev | libatlas-base-dev
Build-Depends-Indep:
 texinfo, texlive-plain-generic, texlive-latex-base
Standards-Version: 4.6.1
Rules-Requires-Root: no
Homepage: https://github.com/blitzpp/blitz
Vcs-Git: https://salsa.debian.org/math-team/blitzxx.git
Vcs-Browser: https://salsa.debian.org/math-team/blitzxx

Package: libblitz0t64
Breaks: libblitz0v5 (<< ${source:Version})
Architecture: any
Section: libs
Depends: ${shlibs:Depends}, ${misc:Depends}
Suggests: libblitz-doc
Conflicts: libblitz0, libblitz0ldbl
X-Time64-Compat: libblitz0v5
Provides: ${t64:Provides}, libblitz0
Replaces: libblitz0v5, libblitz0, libblitz0ldbl
Multi-Arch: same
Description: C++ template class library for scientific computing
 Blitz++ offers a high level of abstraction,
 but performance which rivals Fortran.  The
 current version supports arrays and vectors.
 .
 This package contains the dynamic library.

Package: libblitz0-dev
Architecture: any
Section: libdevel
Depends: libblitz0t64 (= ${binary:Version}), ${misc:Depends}
Suggests: pkg-config, libblitz-doc (= ${source:Version})
Conflicts: libblitz-dev, blitz++
Provides: libblitz-dev, blitz++
Multi-Arch: same
Description: C++ template class library for scientific computing - libdev
 Blitz++ offers a high level of abstraction,
 but performance which rivals Fortran.  The
 current version supports arrays and vectors.
 .
 This package contains the static library
 and header files for  compiling programs with
 blitz++.

Package: libblitz-doc
Architecture: all
Section: doc
Depends: ${misc:Depends}
Suggests: pdf-viewer
Enhances: libblitz0-dev (= ${binary:Version})
Breaks: libblitz0ldbl (<< 1:0.10-2)
Replaces: libblitz0ldbl (<< 1:0.10-2)
Multi-Arch: foreign
Description: C++ template class library for scientific computing - doc
 Blitz++ offers a high level of abstraction,
 but performance which rivals Fortran.  The
 current version supports arrays and vectors.
 .
 This package contains the documentation and examples.
